import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import { Route } from 'react-router-dom';
import Base from '../pages/base/content';

class App extends React.Component {
	render() {
		return (
			<Route path="/" component={Base}/>
		);
	}
}

export default App;