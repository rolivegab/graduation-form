import CFG from './CFG';

export namespace AJAX {

	export const serverURI = CFG.Server + CFG.pagesUrl;

	export interface Response<T> {
		success: boolean;
		response: T;
	}

	/**
	 * Faz uma requisição AJAX, executando a resposta automaticamente através do método automatic.
	 * A resposta do servidor também pode ser enviada a outra função passando-a pelo parâmetro callback.
	 * @param {string[]} header Cabeçalho da requisição AJAX, o padrão é "default".
	 * @param {Function} callback Função que irá receber a resposta da requisição.
	 * @param {string} type Método utilizado para o envio dos parâmetros, o padrão é "POST".
	 * @param {string} file Arquivo do servidor que receberá a requisição, o padrão é o "ClientManager.php".
	 * @param {boolean} sync Define se a execução é assíncrona, ou não. O padrão é "true".
	 */
	export async function request<T>(
		header?: ArrayText, 
		file: string =	'', 
		sync: boolean = true
	): Promise<T> {

		return new Promise<T>((resolve: (aT: T) => void, reject: (err: ErrorEvent) => void) => {
			let xmlhttp = new XMLHttpRequest();

			xmlhttp.onreadystatechange = () => {
				if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
					let responseJSON: any;
					try {
						responseJSON = JSON.parse(xmlhttp.responseText);
						if (responseJSON.redirect) {
							window.location.href = responseJSON.redirect;
						}
						resolve(responseJSON);
					} catch (e) {
						console.log('Error, response: ', xmlhttp.responseText);
					}
				}
			};

			xmlhttp.onerror = (e) => {
				console.log('Erro na conexão: ', e);
			};

			xmlhttp.open('POST', serverURI + file, sync);
			xmlhttp.setRequestHeader('Content-type', 'application/json');
			xmlhttp.withCredentials = true;
			xmlhttp.send(JSON.stringify(header));
		});
	}

	export async function uploadFile<T>(
		header?: ArrayText, 
		file: string =	'', 
		sync: boolean = true
	): Promise<T> {

		return new Promise<T>((resolve: (aT: T) => void, reject: (err: ErrorEvent) => void) => {
			let xmlhttp = new XMLHttpRequest();

			xmlhttp.onreadystatechange = () => {
				if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
					let responseJSON: any;
					try {
						responseJSON = JSON.parse(xmlhttp.responseText);
						if (responseJSON.redirect) {
							window.location.href = responseJSON.redirect;
						}
						resolve(responseJSON);
					} catch (e) {
						console.log('Error, response: ', xmlhttp.responseText);
					}
				}
			};

			xmlhttp.onerror = (e) => {
				console.log('Erro na conexão: ', e);
			};

			xmlhttp.open('POST', serverURI + file, sync);
			xmlhttp.withCredentials = true;
			xmlhttp.send(header);
		});
	}
	
	export type ArrayText = any;
}

export default AJAX;