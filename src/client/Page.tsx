import * as React from 'react';

export default class<P, T> extends React.Component<P, T> {
	style: HTMLStyleElement;

	constructor(props: P) {
		super(props);
	}

	prepareStyle(stylecontent: string) {
		this.style = document.createElement('style');
		this.style.innerHTML = stylecontent;
		document.head.appendChild(this.style);
	}

	removeStyle() {
		this.style.remove();
	}
}