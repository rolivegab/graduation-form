import * as React from 'react';

export interface Props {
	init?: boolean;
	name?: string;
	onClick?: (checked: boolean) => void;
	value?: boolean;
}

export default class extends React.Component<Props> {
	constructor(props: Props) {
		super(props);
		this.state = {
			value: this.props.init ? this.props.init : false
		};

		this.onClickHandle = this.onClickHandle.bind(this);
	}

	onClickHandle(e: React.MouseEvent<HTMLInputElement>) {
		if (this.props.onClick) {
			this.props.onClick(e.currentTarget.checked);
		}
		e.preventDefault();
	}

	render() {
		return (
			<label className="switch">
				<input type="checkbox" name={this.props.name} onClick={this.onClickHandle} checked={this.props.value} onChange={() => (undefined)}/>
				<span className="slider"/>
			</label>
		);
	}
}