import * as React from 'react';

export default (): JSX.Element => (
	<div className="container-fluid text-center logo">
		<img className="img-fluid" src="/image/logo.png" style={{marginLeft: '-100px', marginRight: '-100px', maxWidth: '120%'}}/>
	</div>
);