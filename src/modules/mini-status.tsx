import * as React from 'react';

export default class extends React.Component<{flag?: boolean}> {
	render() {
		if (typeof this.props.flag === 'boolean') {
			return <div className={'font-awesome mini-status' + (this.props.flag ? ' correct' : ' incorrect')}> {this.props.flag ? '\uf00c' : '\uf00d'} </div>;
		} else {
			return null;
		}
	}
}