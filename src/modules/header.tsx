import * as React from 'react';

export default () => (
	<header className="info">
		<b>Contato</b><br/>
		<span className="icon font-awesome">&#xf2b6;</span> suporteead@unifacex.com.br<br/>
		<span className="icon font-awesome">&#xf098;</span> (84) 3235-1415
	</header>
);