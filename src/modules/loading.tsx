import * as React from 'react';

export default (): JSX.Element => (
	<div className="lds-ring"><div/><div/><div/><div/></div>
);