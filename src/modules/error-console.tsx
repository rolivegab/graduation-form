import * as React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
	padding: 10px;
	text-justify: auto;
	margin-bottom: 10px;
	border: 1px solid #ced4da;
	white-space: pre-wrap;

	.close {
		float: right;
		font-size: 21px;
		font-weight: 700;
		line-height: 1;
		color: #000;
		text-shadow: 0 1px 0 #fff;
		filter: alpha(opacity=20);
		opacity: .2;

		&:hover {
			color: #000;
			text-decoration: none;
			cursor: pointer;
			filter: alpha(opacity=50);
			opacity: .5;
		}
	}
}
`;

interface State {
	closed: boolean;
}

interface Props {
	text: string;
}

export default class extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			closed: false
		};
	}

	componentWillReceiveProps(nextProps: Props) {
		this.setState({
			closed: false
		});
	}

	render() {
		return (
			<StyledDiv className="small text-center error-console" hidden={this.props.text.length === 0 || this.state.closed}>
				<span className="float-right"><b>x</b></span>
				<span>{this.props.text}</span>
			</StyledDiv>
		);
	}
}