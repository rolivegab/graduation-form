import { Route, Redirect } from 'react-router';
import * as React from 'react';

export default ({url}: {url: string}) => (
	<Route path={url + '/'} render={() => (<Redirect to={url + '/login'}/>)} exact={true} />
);