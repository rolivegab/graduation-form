import * as React from 'react';
import MiniLoading from './mini-loading';

interface Props {
	text: string;
	isLoading: boolean;
	onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export default class extends React.Component<Props> {
	constructor(props: Props) {
		super(props);
	}

	render() {
		if (this.props.isLoading) {
			return (
				<button className="btn btn-block btn-lg button-loading" onClick={this.props.onClick}>
					<MiniLoading 
						width={40} 
						height={40}
					/>
				</button>
			);
		} else {
			return (
				<button 
					className="btn btn-block btn-lg button-loading"
				>
					{this.props.text}
				</button>
			);
		}
	}
}