import styled from 'styled-components';

export default styled.div`
	.logout-root {
		position: relative;

		.logout {
			position: absolute;
			top: 0;
			right: 0;
			z-index: 1;
		}
	}
`;