import * as React from 'react';
import { AJAX } from '../../../client/Core';
import Loading from '../../../modules/loading';
import { RouteComponentProps, Link } from 'react-router-dom';
import Logo from '../../../modules/unifacexImg';
import StyledDiv from './style';

interface State {
	isUserStatusReceived: boolean;
	isEmailValidated?: boolean;
	hasGraduationInscription: boolean;
	fullname: string;
}

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			isUserStatusReceived: false,
			hasGraduationInscription: false,
			fullname: '-'
		};
	}

	componentWillMount() {
		// Get user INFO
		interface Request {
			success?: boolean;
			logged?: boolean;
			isEmailValidated?: boolean;
			hasGraduationInscription: boolean;
			fullname: string;
		}
		AJAX.request<Request>({
			command: 'user-status'
		}, '/user/panel/').then((aT) => {
			if (aT.success === true) {
				this.setState({
					isUserStatusReceived: true,
					isEmailValidated: aT.isEmailValidated,
					hasGraduationInscription: aT.hasGraduationInscription,
					fullname: aT.fullname
				});
			} else if (aT.success === false) {
				if (aT.logged === false) {
					this.props.history.push('/acesso');
				}
			}
		});
	}

	logout() {
		AJAX.request<{success: boolean}>({
			command: 'logout'
		}, '/user/').then((aT) => {
			if (aT.success) {
				this.props.history.push('/');
			}
		});
	}

	render() {
		return (
			<StyledDiv className="container content text-center user-panel">
				{this.state.isUserStatusReceived ? (
					this.state.isEmailValidated ? (
						<div className="container">
							<div className="row logout-root">
								<h2 className="col-md-12">Painel do Usuário</h2>
								<h5 className="col-md-12">Bem-vindo, {this.state.fullname}!</h5>
								<h5 className="col-md-12"><a href="javascript:void(0)" onClick={e => this.logout()}><small><b>Deslogar</b></small></a></h5>
							</div>
							<br/>
							<br/>
							<div className="row">
								<h4 className="col-md-12 text-left">
									<b>Editais abertos</b>
								</h4>
							</div>
							<div className="row text-left">
								<div className="ml-4 col-md-12">
									<span>Graduação</span><br/>
									{this.state.hasGraduationInscription ? (
										<small>A sua Inscrição já foi realizada.{/* Clique <Link to="/inscricao/graduacao/inicio">aqui</Link> para ver o status.*/}</small>
									) : (
										<small>Você ainda não se inscreveu, clique <Link to="/inscricao/graduacao/inicio">aqui</Link> para se inscrever.</small>
									)}
								</div>
								{/* <Link to="/inscricao/pos-graduacao/inicio"><li>Pós-Graduação</li></Link> */}
							</div>
						</div>
					) : (
						<div>
							<Logo/>
							<h3>Seu e-mail ainda não foi confirmado, favor checar sua caixa de mensagens.</h3>
							<br/>
							<h5>Não recebeu o e-mail de confirmação? <br/>
							Envie um e-mail para o suporte que resolveremos o seu problema.</h5>
						</div>
					)
				) : (
					<div className="container-fluid text-center">
						<Loading/>
					</div>
				)}
			</StyledDiv>
		);
	}
}