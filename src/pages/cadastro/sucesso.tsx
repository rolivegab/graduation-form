import * as React from 'react';
import { Link } from 'react-router-dom';

export default class extends React.Component {
	render() {
		return (
			<div className="content container text-center cadastro-sucesso">
				<h2> Cadastro realizado com sucesso! </h2>
				<h4> Foi enviado uma confirmação para o e-mail cadastrado no sistema. </h4>
				<br/>
				<br/>
				<div className="text-left mb-4 incorrect"><big>ATENÇÃO! Você acaba de cadastrar a sua conta, mas você ainda precisa se inscrever no edital!</big><br/></div>
				<div className="text-left mb-4"><span>Para continuar com o processo de inscrição, faça a confirmação de email e acesse sua conta através através do botão "ACESSO" localizado na página inicial.</span></div>
				<div className="text-left mb-4">Se você tiver alguma dúvida, entre em contato conosco via e-mail: <b>suporteead@unifacex.com.br</b></div>
				<Link to={'/acesso'}><button className="btn btn-lg">Voltar</button></Link>
			</div>
		);
	}
}