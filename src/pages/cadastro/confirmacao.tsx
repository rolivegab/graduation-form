import * as React from 'react';
import Logo from '../../modules/unifacexImg';
import * as queryString from 'query-string';
import AJAX from '../../client/Core';
import Loading from '../../modules/loading';
import { Link } from 'react-router-dom';

interface State {
	confirmed: boolean;
	isAlreadyValidated: boolean;
}

export default class extends React.Component<{}, State> {
	token: boolean;

	constructor(props: {}) {
		super(props);

		this.token = queryString.parse(location.search).token,

		this.state = {
			confirmed: false,
			isAlreadyValidated: false
		};
	}

	componentDidMount() {
		if (this.token) {
			interface Request {
				success: boolean;
			}
			AJAX.request<Request>(
				{
					token: this.token
				},
				'/cadastro/confirmacao/'
			).then((aT) => {
				if (aT.success === true) {
					this.setState({
						confirmed: true
					});
				} else {
					this.setState({
						confirmed: true,
						isAlreadyValidated: true
					});
				}
			});
		}
	}

	render() {
		return (
			<div className="container content">
				<Logo/>
				{this.state.confirmed ? (
					<div>
						<div className="container-fluid text-center">
							{this.state.isAlreadyValidated ? (
								'O seu email já foi validado!'
							) : (
								'Seu cadastro foi confirmado com sucesso!'
							)}
						</div>
						<br/>
						<div className="container-fluid text-center">
							<Link to="/acesso">
								<button className="btn btn-lg"> Continuar </button>
							</Link>
						</div>
					</div>
				) : (
					<div className="container-fluid text-center">
						<Loading/>
					</div>
				)}
			</div>
		);
	}
}