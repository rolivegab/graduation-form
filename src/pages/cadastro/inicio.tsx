import * as React from 'react';
import Logo from '../../modules/unifacexImg';
import Loading from '../../modules/loading';
import { AJAX } from '../../client/Core';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

interface State {
	isInscriptionOpen?: boolean;
}

const StyledDiv = styled.div`
	@media (min-width: 992px) {
		&.container{
			max-width: 960px;
		}
	}
`;

export default class extends React.Component<{}, State> {

	constructor(props: {}) {
		super(props);

		this.state = {};
	}

	componentWillMount() {
		AJAX.request<{isInscriptionOpen: boolean}>(
			{
				command: 'isInscriptionOpen'
			},
			'/cadastro/inicio/'
		).then((aT) => {
			if (typeof aT.isInscriptionOpen === 'boolean') {
				this.setState({
					isInscriptionOpen: aT.isInscriptionOpen
				});
			}
		});
	}

	renderUnknow() {
		return (
			<div className="container-fluid text-center"><Loading/></div>
		);
	}

	renderOK() {
		return (
			<div>
				<Logo/>
				<p className="text-justify">Preencha o formulário a seguir e siga os passos para realizar seu cadastro no nosso sistema de gerenciamento e submissão de conteúdo.</p>
				{/* <br/> */}
				{/* <div>Acesse o edital clicando <a className="link" href="/file/edital.pdf" target="_blank">aqui</a>.</div> */}
				<br/>
				<div className="mb-4">Se você tiver alguma dúvida, entre em contato via e-mail: <b>ead@unifacex.com.br</b></div>
				<br/>
				<Link className="clear" to="conta"><button className="btn btn-block btn-lg">Iniciar inscrição</button></Link>
			</div>
		);
	}

	renderOutOfPeriod() {
		return (
			<div className="container-fluid text-center">
				<Logo/>
				Formulário fechado para inscrição.
			</div>
		);
	}

	renderContent() {
		switch (this.state.isInscriptionOpen) {
			case false:
				return this.renderOutOfPeriod();
			case true:
				return this.renderOK();
			default:
				return this.renderUnknow();
		}
	}

	render() {
		return (
			<StyledDiv className="container content">
				{this.renderContent()}
			</StyledDiv>
		);
	}
}