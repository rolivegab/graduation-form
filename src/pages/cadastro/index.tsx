import * as React from 'react';
import { Switch, Route, RouteComponentProps, Redirect } from 'react-router-dom';
import Inicio from './inicio';
import Dados from './dados';
import Conta from './conta';
import Confirmacao from './confirmacao';
import Sucesso from './sucesso';

export default class extends React.Component<RouteComponentProps<{}>> {
	render() {
		return (
			<Switch>
				<Route path={this.props.match.url + '/inicio'} component={Inicio}/>
				<Route path={this.props.match.url + '/dados'} component={Dados}/>
				<Route path={this.props.match.url + '/conta'} component={Conta}/>
				<Route path={this.props.match.url + '/confirmacao'} component={Confirmacao}/>
				<Route path={this.props.match.url + '/sucesso'} component={Sucesso}/>
				<Redirect from={this.props.match.url} to={this.props.match.url + '/inicio'} exact={true}/>
				<Redirect to={'/not-found'}/>
			</Switch>
		);
	}
}