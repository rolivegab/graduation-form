import * as React from 'react';
import Logo from '../../modules/unifacexImg';
import AJAX from '../../client/Core';
import * as cpf from 'cpf';
import * as queryString from 'query-string';
import MiniStatus from '../../modules/mini-status';
import LoadingButton from '../../modules/loading-button';
import { RouteComponentProps } from 'react-router-dom';
import TextMask from 'react-text-mask';
import ErrorConsole from '../../modules/error-console';
import styled from 'styled-components';

interface Props extends RouteComponentProps<any> {
}

interface State {
	cpf: {
		value: string;
		isValid?: boolean;
	};
	email: {
		value: string;
		confValue: string;
		isValid?: boolean;
	};
	submitLoading: boolean;
	errorMsg: string;
}

const StyledDiv = styled.div`
	width: auto;

	form {
		margin-top: 20px;
	}

	button {
		margin-top: 20px;
	}
`;

export default class extends React.Component<Props, State> {

	errorElementText: HTMLLinkElement;

	constructor(props: Props) {
		super(props);

		this.state = {
			cpf: {
				value: '',
				isValid: false
			},
			email: {
				value: '',
				confValue: '',
				isValid: false
			},
			submitLoading: false,
			errorMsg: ''
		};
	}

	cpfOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState(prevState => ({
			cpf: {
				value: value,
				isValid: cpf.isValid(value)
			}
		}));
	}

	emailOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		let el = e.currentTarget;
		this.setState(prevState => ({
			email: {
				...prevState.email,
				value: el.value,
				isValid: /.+\@.+\..+/.test(el.value)
			}
		}));
	}

	emailConfOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState(prevState => ({
			email: {
				...prevState.email,
				confValue: value
			}
		}));
	}

	isCPFVerified() {
		let q = queryString.parseUrl(document.URL).query;

		if (q.cpf === '0') {
			return false;
		} else {
			return true;
		}
	}

	showMessage = (text: string) => {
		this.setState({
			errorMsg: text
		});
	}

	onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		if (!this.state.cpf.isValid) {
			this.showMessage('CPF inválido.');
			return false;
		}

		if (!this.state.email.isValid) {
			this.showMessage('Email inválido.');
			return false;
		}

		if (!(this.state.email.value === this.state.email.confValue)) {
			this.showMessage('Confirmação de Email diferente do Email');
			return false;
		}

		this.setState({
			submitLoading: true
		});

		return AJAX.request<{success: boolean, response: {errorMsg: string}}>(
			{
				command: 'create-user',
				cpf: cpf.clear(this.state.cpf.value),
				email: this.state.email.value
			},
			'/cadastro/conta/'
		).then((aT) => {
			this.setState({
				submitLoading: false
			});

			if (aT.success === true) {
				this.props.history.push('/cadastro/dados');
				return true;
			} else {
				this.setState({
					errorMsg: aT.response.errorMsg
				});
				return false;
			}
		});
	}

	render() {
		return (
			<StyledDiv className="container content">
				<Logo/>
				<form action="javascript:void(0)" onSubmit={this.onSubmit}>
					<ErrorConsole text={this.state.errorMsg}/>
					<label>
						<TextMask
							className="form-control form-control-lg" 
							type="text" 
							name="cpf" 
							id="cpf"
							placeholder="CPF" 
							required={true}
							value={this.state.cpf.value}
							onChange={this.cpfOnChange}
							mask={[/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/]}
						/>
					</label>
					<MiniStatus flag={this.state.cpf.isValid}/>
					<br/>

					<label>
						<input
							className="form-control form-control-lg"
							type="email" 
							name="email" 
							id="email"
							placeholder="Email"
							value={this.state.email.value}
							onInput={this.emailOnInput}
							maxLength={255}
						/>
					</label>
					<MiniStatus flag={this.state.email.isValid}/>
					<br/>

					<label>
						<input 
							className="form-control form-control-lg"
							type="emailConf" 
							name="emailConf" 
							id="emailConf"
							placeholder="Confirmar Email"
							value={this.state.email.confValue}
							onInput={this.emailConfOnInput}
							maxLength={255}
						/>
					</label>
					<MiniStatus flag={this.state.email.value === this.state.email.confValue}/>
					<br/>

					<LoadingButton 
						isLoading={this.state.submitLoading} 
						text={'Próximo'}
					/>
				</form>
			</StyledDiv>
		);
	}
}