import * as React from 'react';
import LoadingButton from '../../modules/loading-button';
import { AJAX } from '../../client/Core';
import TextMask from 'react-text-mask';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';

interface State {
	submitLoading: boolean;
	name: string;
	rg: string;
	birthday: string;
	address: string;
	number: string;
	cep: string;
	city: string;
	tel: string;
}

const StyledDiv = styled.div`
	&.container {
		width: auto;
	}
`;

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			submitLoading: false,
			name: '',
			rg: '',
			birthday: '',
			address: '',
			number: '',
			cep: '',
			city: '',
			tel: ''
		};
	}

	onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		this.setState({
			submitLoading: true
		});

		AJAX.request(
			{
				command: 'create-user',
				...this.state,
				cep: this.state.cep.replace(/-/, ''),
				tel: this.state.cep.replace(/[\(\)\s\-]/, ''),
				submitLoading: undefined
			},
			'/cadastro/dados/'
		).then((aT) => {
			this.setState({
				submitLoading: false
			});
			this.props.history.push('/cadastro/sucesso');
		});
	}

	completeNameOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			name: e.currentTarget.value
		});
	}

	rgOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			rg: e.currentTarget.value
		});
	}

	birthdayOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			birthday: e.currentTarget.value
		});
	}

	addressOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			address: e.currentTarget.value
		});
	}

	numberOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			number: e.currentTarget.value
		});
	}

	cepOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			cep: e.currentTarget.value
		});
	}

	cityOnInput = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			city: e.currentTarget.value
		});
	}

	telOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			tel: e.currentTarget.value
		});
	}

	telMask = (text: string) => {
		let tel = text.replace('(', '').replace(')', '').replace('-', '').replace(' ', '').replace(/_/g, '');
		if (tel.length > 10) {
			return ['(', /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
		} else {
			return ['(', /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
		}
	}

	render() {
		return (
			<StyledDiv className="content container cadastro-dados">
				<h2>Dados do Usuário</h2>
				<br/><br/>

				<form action="javascript:void(0)" onSubmit={this.onSubmit}>
					<label>
						<input 
							className="form-control form-control-lg" 
							type="text" 
							placeholder="Nome completo"
							onInput={this.completeNameOnInput}
							value={this.state.name}
							maxLength={255}
						/>
					</label><br/>
					<label>
						<input 
							className="form-control form-control-lg" 
							type="text" 
							placeholder="RG"
							onInput={this.rgOnInput}
							value={this.state.rg}
							maxLength={15}
						/>
					</label><br/>
					<label>
						<TextMask
							className="form-control form-control-lg" 
							type="text" 
							placeholder="Data de nascimento"
							onChange={this.birthdayOnInput}
							mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
							value={this.state.birthday}
						/>
						<small className="text-muted">No formato DD/MM/AAAA</small>
					</label><br/>
					<label>
						<input 
							className="form-control form-control-lg" 
							type="text" 
							placeholder="Endereço"
							onInput={this.addressOnInput}
							value={this.state.address}
							maxLength={255}
						/>
					</label><br/>
					<label>
						<input 
							className="form-control form-control-lg" 
							type="text" 
							placeholder="Número"
							onInput={this.numberOnInput}
							maxLength={20}
						/>
					</label><br/>
					<label>
						<TextMask
							className="form-control form-control-lg" 
							type="text" 
							placeholder="CEP"
							onChange={this.cepOnInput}
							mask={[/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
							value={this.state.cep}
						/>
					</label><br/>
					<label>
						<input 
							className="form-control form-control-lg" 
							type="text" 
							placeholder="Cidade"
							onInput={this.cityOnInput}
							value={this.state.city}
							maxLength={50}
						/>
					</label><br/>
					<label>
						<TextMask
							className="form-control form-control-lg" 
							type="text" 
							placeholder="Telefone"
							mask={this.telMask}
							onChange={this.telOnChange}
							value={this.state.tel}
						/>
					</label><br/>
					
					<LoadingButton 
						isLoading={this.state.submitLoading} 
						text={'Próximo'}
					/>
				</form>
			</StyledDiv>
		);
	}
}