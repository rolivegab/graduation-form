import * as React from 'react';
import { Route, RouteProps, Switch, Redirect } from 'react-router';
import Home from '../home/content';
import Cadastro from '../cadastro/index';
import Acesso from '../acesso/content';
import UserPanel from '../user/panel/content';
import Inscricao from '../inscricao/graduacao/index';
import EnviarConteudo from '../enviar-conteudo/content';
import ViewSubmissions from '../view-submissions/content';
import NotFound from '../not-found/content';
import Admin from '../admin/content';
import EnvioSucesso from '../envio-sucesso/content';
import AJAX from '../../client/Core';
import Loading from '../../modules/loading';
import Header from '../../modules/header';
import pathMatch from 'path-match';
import * as url from 'url';

interface State {
	pages: RouteProps[];
	redirect: string;
	isInMaintenance?: boolean;
}

export default class extends React.Component<{}, State> {
	constructor(props: {}) {
		super(props);
		this.state = {
			pages: [
				{path: '/', component: Home, exact: true},
				{path: '/cadastro', component: Cadastro},
				{path: '/acesso', component: Acesso, exact: true},
				{path: '/user/panel', component: UserPanel, exact: true},
				{path: '/inscricao/graduacao/', component: Inscricao},
				{path: '/not-found', component: NotFound, exact: true},
				{path: '/admin', component: Admin, exact: true},
				{path: '/enviar-conteudo', component: EnviarConteudo, exact: true},
				{path: '/view-submissions', component: ViewSubmissions, exact: true},
				{path: '/envio-sucesso', component: EnvioSucesso, exact: true},
				{path: '/admin', component: Admin}
			],
			redirect: '/not-found'
		};
	}

	componentWillMount() {
		let route = pathMatch({
			sensitive: false,
			strict: false,
			end: false
		});

		let match = route('/admin');
		let params = match(url.parse(document.URL).pathname);

		if (params !== false) {
			this.setState({
				isInMaintenance: false
			});
		} else {
			AJAX.request<{isInMaintenance: boolean}>(
				{
					command: 'isInMaintenance'
				},
				'/base/'
			).then((aT) => {
				console.log(aT);
				if (typeof aT.isInMaintenance === 'boolean') {
					this.setState({
						isInMaintenance: aT.isInMaintenance
					});
				}
			});
		}
	}

	renderContent() {
		switch (this.state.isInMaintenance) {
			case true:
				return (
					<div className="container content text-center">
						<h3> Site em manutenção. </h3>
					</div>
				);
			case false:
				return (
					<Switch>
						{this.state.pages.map((i, indexI) => {
							return <Route key={indexI} {...i}/>;
						})}
						<Redirect to={this.state.redirect}/>
					</Switch>
				);
			default:
				return <div className="content container text-center"><Loading/></div>;
		}
	}

	render() {
		return (
			<div className="container-fluid base">
				<div className="row justify-content-center">
					<Header/>
					{this.renderContent()}
				</div>
			</div>
		);
	}
}