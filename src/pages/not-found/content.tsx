import * as React from 'react';

export default class extends React.Component {
	render() {
		return (
			<div className="container content not-found">
				<h1>404: Página não encontrada.</h1>
			</div>
		);
	}
}