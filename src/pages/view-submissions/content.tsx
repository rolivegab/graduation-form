import * as React from 'react';
import { AJAX } from '../../client/Core';
import './view-submissions.css';

interface Area {
	id: number;
	name: string;
	courses: Course[];
}

interface Course {
	id: number;
	name: string;
	submissions: Submission[];
}

interface Submission {
	cpf: string;
	name: string;
	email: string;
}

interface State {
	areas: Area[];
	submissionCount: string;
}

export default class extends React.Component<{}, State> {

	constructor(props: {}) {
		super(props);

		this.state = {
			areas: [],
			submissionCount: '-'
		};

		this.getAreas = this.getAreas.bind(this);
		this.getCourse = this.getCourse.bind(this);
		this.getSubmissions = this.getSubmissions.bind(this);
		this.renderAreas = this.renderAreas.bind(this);
		this.renderCourses = this.renderCourses.bind(this);
		this.renderSubmissions = this.renderSubmissions.bind(this);
		this.getSubmissionCount = this.getSubmissionCount.bind(this);
	}

	getSubmissions(areapos: number, coursepos: number, courseid: number) {
		AJAX.request<{submissions: Submission[]}>(
			{
				command: 'submission',
				courseid: courseid
			},
			'/view-submissions'
		).then((aT) => {
			if (!aT.submissions) {
				return;
			}

			let submissions = aT.submissions;

			this.setState(prevState => {
				prevState.areas[areapos].courses[coursepos].submissions = submissions;

				return prevState;
			});
		});
	}

	getCourse(areapos: number, areaid: number) {
		AJAX.request<{courses: Course[]}>(
			{
				command: 'course',
				areaid: areaid
			},
			'/view-submissions'
		).then((aT) => {
			if (!aT.courses) {
				return;
			}

			let courses = aT.courses;

			this.setState(prevState => {
				prevState.areas[areapos].courses = courses;

				return prevState;
			});

			courses.forEach((i, indexI) => {
				this.getSubmissions(areapos, indexI, i.id);
			});
		});
	}

	getAreas() {
		// Pega cada área:
		AJAX.request<{areas: Area[]}>(
			{
				command: 'area'
			}, 
			'/view-submissions'
		).then((aT) => {
			if (!aT.areas) {
				return;
			}

			let areas = aT.areas;

			this.setState({
				areas: areas
			});

			// Pega cada curso:
			areas.forEach((i, indexI) => {
				this.getCourse(indexI, i.id);
			});
		});
	}

	renderAreas() {
		return this.state.areas.map((i, indexI) => {
			return (
				<div key={indexI} data-areaid={i.id}>
					<span>{i.name}</span>
					{this.renderCourses(indexI)}
				</div>
			);
		});
	}

	renderCourses(areaid: number) {
		if (!this.state.areas[areaid].courses) {
			return null;
		}
		return this.state.areas[areaid].courses.map((j, indexJ) => {
			return (
				<div key={indexJ} data-courseid={j.id}>
					<span>{j.name}</span>
					<table>
						<tbody>
							{this.renderSubmissions(areaid, indexJ)}
						</tbody>
					</table>
				</div>
			);
		});
	}

	renderSubmissions(areaid: number, courseid: number) {
		if (!this.state.areas[areaid].courses[courseid].submissions) {
			return null;
		}

		return this.state.areas[areaid].courses[courseid].submissions.map((k, indexK) => {
			return (
				<tr key={indexK} onClick={undefined} data-submissioncpf={k.cpf}>
					<td>{k.cpf}</td>
					<td>{k.name}</td>
					<td>{k.email}</td>
				</tr>
			);
		});
	}

	getSubmissionCount() {
		AJAX.request<{submissioncount: any}>(
			{
				command: 'submission-count'
			},
			'/view-submissions'
		).then((aT) => {
			if (aT.submissioncount) {
				this.setState({
					submissionCount: aT.submissioncount.count
				});
			}
		});
	}

	componentWillMount() {
		this.getAreas();
		this.getSubmissionCount();
	}

	render() {
		return (
			<div className="container content view-submission">
				<div className="container-fluid text-center"><h2>ADMINISTRAÇÃO</h2></div><br/>
				<br/>
				<strong><span> Total de submissões: {this.state.submissionCount} </span></strong>
					{this.renderAreas()}
			</div>
		);
	}
}