import * as React from 'react';
import Logo from '../../modules/unifacexImg';
import { AJAX } from '../../client/Core';
import './enviar-conteudo.css';
import CFG from '../../client/CFG';

interface State {
	cpf: string;
	name: string;
}

export default class extends React.Component<{}, State> {

	constructor(props: {}) {
		super(props);

		this.state = {
			cpf: '-',
			name: '-'
		};

		this.fileInputOnChange = this.fileInputOnChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentWillMount() {
		AJAX.request<{cpf: string, name: string}>(
			{
				command: 'user-info'
			},
			'/enviar-conteudo/'
		).then((aT) => {
			if (!aT.cpf || !aT.name) {
				return;
			}
			this.setState({
				cpf: aT.cpf,
				name: aT.name
			});
		});
	}

	fileInputOnChange(e: React.ChangeEvent<HTMLInputElement>) {
		let value = e.currentTarget.value;
		let label = document.querySelector('label[for=customFile]');
		if (label) {
			if (value) {
				label.innerHTML = value;
			} else {
					label.innerHTML = 'Selecionar arquivo';
			}
		}
	}

	onSubmit(e: React.FormEvent<HTMLFormElement>) {
		let form = e.currentTarget;
		if (form.customFile) {
			let customFile = form.customFile as HTMLInputElement;
			if (customFile.files) {
				let file = customFile.files[0];
				if (file) {
					if (file.size < CFG.maxEnviarArquivoSize * 1024 * 1024) {
						return;
					}
				}
			}
		}

		e.preventDefault();
	}

	render() {
		return (
			<div className="container content enviar-conteudo">
				<Logo/>
				<form className="form-group" action={AJAX.serverURI + '/enviar-conteudo/'} encType="multipart/form-data" method="POST" onSubmit={this.onSubmit}>
					<h5>Bem vindo, <b>{this.state.name}</b>!</h5>
					<p className="small text-muted">Selecione um arquivo no formato <b>.zip</b>, <b>.rar</b>, ou <b>.tar.gz</b> com o conteúdo pedido.</p>
					<div className="custom-file">
						<input className="custom-file-input" id="customFile" type="file" name="arquivo" onChange={this.fileInputOnChange} aria-describedby="customFileLabel"/>
						<label className="custom-file-label" htmlFor="customFile">Selecionar arquivo</label>
					</div><small className="form-text" id="customFileLabel">Tamanho máximo do arquivo: {CFG.maxEnviarArquivoSize}MB.<span className="text-danger">*</span></small>
					<br/>
					<br/>
					<button className="btn btn-lg btn-block"> Enviar arquivo </button>
				</form>
			</div>
		);
	}
}