import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import LoadingButton from '../../../modules/loading-button';
import { RouteComponentProps } from 'react-router-dom';
import { AJAX } from '../../../client/Core';
import ErrorConsole from '../../../modules/error-console';
import styled from 'styled-components';

interface State {
	isRequesting: boolean;
	roles: string[];
	errorMsg: string;
}

const StyledDiv = styled.div`
	&.content {
		width: auto;
	}
`;

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			isRequesting: false,
			roles: [],
			errorMsg: ''
		};
	}

	onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		interface Request {
			success?: boolean;
		}

		AJAX.request<Request>({
			command: 'submit-roles',
			roles: this.state.roles
		}, '/inscricao/graduacao/vaga/').then((aT) => {
			if (aT.success === true) {
				this.props.history.push('disciplinas');
			}
		});

		return true;
	}

	roles = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		if (value === 'conteudist') {
			this.setState({
				roles: ['conteudist']
			});
		} else if (value === 'reviewer') {
			this.setState({
				roles: ['reviewer']
			});
		} else if (value === 'both') {
			this.setState({
				roles: ['conteudist', 'reviewer']
			});
		}
	}

	render() {
		return (
			<StyledDiv className="content container">
				<Logo/>
				<div className="container-fluid text-center">
					<h2>Vaga</h2>
					<h5>Selecione a vaga para a qual gostaria de se candidatar</h5>
					<small>No mínimo 1, no máximo 3</small>
				</div>
				<br/>
				<ErrorConsole text={this.state.errorMsg}/>
				<form action="javascript:void(0)" onSubmit={this.onSubmit}>
				<big className="text-block mb-2" style={{display: 'block'}}>Gostaria de se candidatar para qual vaga?</big>
					<div className="form-group control-group">
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input
										type="radio" 
										id="conteudist"
										value="conteudist"
										name="roles"
										onChange={this.roles} 
										checked={this.state.roles.length === 1 && this.state.roles[0] === 'conteudist'}
										required={true}
									/>
								</div>
							</div>
							<label htmlFor="conteudist" className="form-control"> Conteudista </label><br/>
						</div>
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input 
										type="radio" 
										id="reviewer"
										value="reviewer"  
										name="roles"
										onChange={this.roles} 
										checked={this.state.roles.length === 1 && this.state.roles[0] === 'reviewer'}
									/>
								</div>
							</div>
							<label htmlFor="reviewer" className="form-control"> Professor revisor </label>
						</div>
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input 
										type="radio" 
										id="both"
										value="both"  
										name="roles"
										onChange={this.roles} 
										checked={this.state.roles.length === 2}
									/>
								</div>
							</div>
							<label htmlFor="both" className="form-control"> Ambos </label>
						</div>
					</div>
					<LoadingButton 
						isLoading={this.state.isRequesting}
						text="Próximo"
					/>
				</form>
			</StyledDiv>
		);
	}
}