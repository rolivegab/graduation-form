import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';
import LoadingButton from '../../../modules/loading-button';
import AJAX from '../../../client/Core';

interface State {
	employeedBefore: string;
	teacherExperience: string;
	eadExperience: string;
	contentExperience: string;
	otherExperiences: string;
	isRequesting: boolean;
}

const StyledDiv = styled.div`
	.control-group {
		.input-group:not(:last-child) {
			label, .input-group-text {	
				border-bottom: 0;
			}
		}
	}
`;

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			isRequesting: false,
			employeedBefore: '',
			contentExperience: '',
			eadExperience: '',
			otherExperiences: '',
			teacherExperience: ''
		};
	}

	onRadioChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			employeedBefore: value
		});
	}

	onSubmit = () => {
		this.setState({
			isRequesting: true
		});

		AJAX.request<{success: boolean}>({
			command: 'submit',
			employeedBefore: this.state.employeedBefore,
			teacherExperience: this.state.teacherExperience,
			eadExperience: this.state.eadExperience,
			contentExperience: this.state.contentExperience,
			otherExperiences: this.state.otherExperiences
		}, '/inscricao/graduacao/profissional/').then((aT) => {
			if (aT.success) {
				this.props.history.push('arquivos');
			}
		});
	}

	teacherExperience = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			teacherExperience: value
		});
	}

	eadExperience = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			eadExperience: value
		});
	}

	contentExperience = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			contentExperience: value
		});
	}

	otherExperiences = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			otherExperiences: value
		});
	}

	render() {
		return (
			<StyledDiv className="content container inscricao-graduacao-academico">
				<Logo/>
				<div className="container-fluid text-center mb-4">
					<h2>Dados profissionais</h2>
					<h5>Preencha o formuláro a seguir com as informações pedidas</h5>
				</div>
				<form action="javascript:void(0)" onSubmit={this.onSubmit}>
					<big className="text-block mb-2" style={{display: 'block'}}>Já foi funcionário ou colaborador da UNIFACEX ?</big>
					<div className="form-group control-group">
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input
										type="radio" 
										id="yes"
										value="yes"
										name="employeedBefore"
										onChange={this.onRadioChange} 
										checked={this.state.employeedBefore === 'yes'}
										required={true}
									/>
								</div>
							</div>
							<label htmlFor="yes" className="form-control"> Sim </label><br/>
						</div>
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input 
										type="radio" 
										id="no"
										value="no"  
										name="employeedBefore"
										onChange={this.onRadioChange} 
										checked={this.state.employeedBefore === 'no'}
									/>
								</div>
							</div>
							<label htmlFor="no" className="form-control"> Não </label>
						</div>
					</div>
					<div className="form-group">
						<label htmlFor="teacherExperience">Detalhamento da experiência como docente:</label>
						<textarea
							className="form-control" 
							id="teacherExperience"
							rows={3}
							onChange={this.teacherExperience}
							value={this.state.teacherExperience}
							maxLength={1000}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="eadExperience">Detalhamento da experiência EAD:</label>
						<textarea
							className="form-control" 
							id="eadExperience"
							rows={3}
							onChange={this.eadExperience}
							value={this.state.eadExperience}
							maxLength={1000}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="contentExperience">Detalhamento da experiência na produção de conteúdo:</label>
						<textarea
							className="form-control" 
							rows={3}
							id="contentExperience"
							onChange={this.contentExperience}
							value={this.state.contentExperience}
							maxLength={1000}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="otherExperiences">Outras experiências relevantes:</label>
						<textarea 
							className="form-control"
							id="otherExperiences"
							rows={3}
							onChange={this.otherExperiences}
							value={this.state.otherExperiences}
							maxLength={1000}
						/>
					</div>
					<br/>
					<LoadingButton 
						isLoading={this.state.isRequesting}
						text="Próximo"
					/>
				</form>
			</StyledDiv>
		);
	}
}