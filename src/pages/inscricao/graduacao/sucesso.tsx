import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import { Link } from 'react-router-dom';

export default class extends React.Component {
	render() {
		return (
			<div className="content container cadastro-graduation-inicio">
				<Logo/>
				<div className="text-center"><h2>Cadastro realizado com sucesso!</h2>
				<big>Um e-mail de confirmação foi enviado para você.</big><br/>
				<span>Em breve você poderá acessar com detalhes as informações que enviou através do painel do usuário.</span></div>
				<br/>
				<Link className="clear" to="/user/panel"><button className="btn btn-block btn-lg">Voltar</button></Link>
			</div>
		);
	}
}