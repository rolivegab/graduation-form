import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';
import LoadingButton from '../../../modules/loading-button';
import AJAX from '../../../client/Core';

interface State {
	bestDegree: string;
	graduationFor: string;
	posgraduationFor: string;
	isRequesting: boolean;
}

const StyledDiv = styled.div`
	.control-group {
		.input-group:not(:last-child) {
			label, .input-group-text {	
				border-bottom: 0;
			}
		}
	}
`;

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			isRequesting: false,
			bestDegree: '',
			graduationFor: '',
			posgraduationFor: ''
		};
	}

	onRadioChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			bestDegree: value
		});
	}

	onSubmit = () => {
		this.setState({
			isRequesting: true
		});

		AJAX.request<{success: boolean}>({
			command: 'submit',
			graduationFor: this.state.graduationFor,
			posgraduationFor: this.state.posgraduationFor,
			bestDegree: this.state.bestDegree
		}, '/inscricao/graduacao/academico/').then((aT) => {
			if (aT.success) {
				this.props.history.push('profissional');
			}
		});
	}

	graduationForOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			graduationFor: value
		});
	}

	posgraduationForOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			posgraduationFor: value
		});
	}

	render() {
		return (
			<StyledDiv className="content container inscricao-graduacao-academico">
				<Logo/>
				<div className="container-fluid text-center">
					<h2>Dados acadêmicos</h2>
					<h5>Preencha o formuláro a seguir com as informações pedidas</h5>
				</div>
				<form action="javascript:void(0)" onSubmit={this.onSubmit}>
					<big className="text-block mb-2" style={{display: 'block'}}>Seu maior nível:</big>
					<div className="form-group control-group">
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input
										type="radio" 
										name="bestDegree"
										id="graduation"
										value="graduation" 
										onChange={this.onRadioChange} 
										checked={this.state.bestDegree === 'graduation'}
										required={true}
									/>
								</div>
							</div>
							<label htmlFor="graduation" className="form-control"> Graduação </label><br/>
						</div>
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input 
										type="radio" 
										name="bestDegree"
										id="specialization"
										value="specialization"  
										onChange={this.onRadioChange} 
										checked={this.state.bestDegree === 'specialization'}
									/>
								</div>
							</div>
							<label htmlFor="specialization" className="form-control"> Especialização </label>
						</div>
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<div className="input-group-text">
									<input 
										type="radio" 
										name="bestDegree" 
										id="master"
										value="master" 
										onChange={this.onRadioChange} 
										checked={this.state.bestDegree === 'master'}
									/>
								</div>
							</div>
							<label htmlFor="master" className="form-control"> Mestre </label>
						</div>
						<div className="input-group input-group-lg">
							<div className="input-group-prepend">
								<label className="input-group-text no-radius">
									<input 
										type="radio" 
										name="bestDegree" 
										id="doctorade"
										value="doctorade" 
										onChange={this.onRadioChange} 
										checked={this.state.bestDegree === 'doctorade'}
									/>
								</label>
							</div>
							<label htmlFor="doctorade" className="form-control"> Doutorado </label>
						</div>
					</div>
					<div className="form-group">
						<label htmlFor="graduationFor">Graduação em:</label>
						<input 
							className="form-control" 
							type="text" 
							name="graduationFor" 
							id="graduationFor"
							onChange={this.graduationForOnChange}
							value={this.state.graduationFor}
							required={true}
							maxLength={150}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="posgraduationFor">Pos-Graduação em (Maior nível, se houver):</label>
						<input 
							className="form-control" 
							type="text" 
							name="posgraduationFor" 
							id="posgraduationFor"
							onChange={this.posgraduationForOnChange}
							value={this.state.posgraduationFor}
							maxLength={150}
						/>
					</div>
					<br/>
					<LoadingButton 
						isLoading={this.state.isRequesting}
						text="Próximo"
					/>
				</form>
			</StyledDiv>
		);
	}
}