import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import { Link } from 'react-router-dom';

export default class extends React.Component {
	render() {
		return (
			<div className="content container cadastro-graduation-inicio">
				<Logo/>
				<p className="text-justify">Bem-vindo ao sistema de inscrição do processo seletivo para professor conteudista e professor revisor de disciplinas EAD – Unifacex. Nos próximos passos serão demandados os documentos e informações previstos no edital 01/2018 do NEaD - Unifacex.</p>
				<br/>
				<div>Acesse o edital clicando <a className="link" href="/file/Edital - Cadastro de Reserva - Professor Conteudista Graduação.pdf" target="_blank">aqui</a>.</div>
				<br/>
				<div className="mb-4">Se você tiver alguma dúvida, entre em contato via e-mail: <b>ead@unifacex.com.br</b></div>
				<br/>
				<Link className="clear" to="vaga"><button className="btn btn-block btn-lg">Iniciar inscrição</button></Link>
			</div>
		);
	}
}