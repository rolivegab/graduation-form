import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';
import LoadingButton from '../../../modules/loading-button';
import AJAX from '../../../client/Core';
import ConsoleError from '../../../modules/error-console';

interface State {
	isRequesting: boolean;
	lattes?: File;
	rg?: File;
	cpf?: File;
	elector?: File;
	residence?: File;
	graduationFront?: File;
	graduationVerse?: File;
	posgraduationFront?: File;
	posgraduationVerse?: File;
	otherFiles: {
		file?: File
	}[];
	isGraduationBothFilesInOne: boolean;
	isPosgraduationBothFilesInOne: boolean;
	hasPosgraduation: boolean;
	errorMsg: string;
}

const StyledDiv = styled.div`
	.form-group {
		font-size: 16px;
		label.custom-file-label::after {
			content: "Selecionar arquivo..."
		}
	}
`;

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			isRequesting: false,
			otherFiles: [],
			isGraduationBothFilesInOne: false,
			isPosgraduationBothFilesInOne: false,
			hasPosgraduation: true,
			errorMsg: ''
		};
	}

	onSubmit = (form: React.FormEvent<HTMLFormElement>) => {
		let fd = new FormData();

		let flag = Object.keys(this.state).every((i, index) => {
			let file = this.state[i];
			if (file && file instanceof File) {
				let fileExtensionName = file.name.split('.').pop();
				let fileSize = file.size;
				if (fileExtensionName && fileExtensionName.toLowerCase() !== 'pdf') {
					this.setState({
						errorMsg: 'O arquivo \'' + file.name + '\' não possui extensão correta.\nSó é permitido enviar arquivos em PDF.'
					});
					return false;
				} else if (fileSize  > 5 * 1024 * 1024) {
					this.setState({
						errorMsg: 'O arquivo \'' + file.name + '\' é maior do que o tamanho máximo permitido.\nSó é permitido enviar arquivos de até 5MB.'
					});
					return false;
				}
				fd.append(i, file);
			} else if (i === 'otherFiles') {
				let fl = file as State['otherFiles'];
				for (let j = 0; j < fl.length; ++j) {
					let item = fl[j];
					if (item.file) {
						fd.append('otherFiles' + (j + 1), item.file);
					}
				}
			}
			
			return true;
		});

		if (!flag) {
			return false;
		}

		this.setState({
			isRequesting: true
		});

		interface Request {
			success: boolean;
		}

		AJAX.uploadFile<Request>(fd, '/inscricao/graduacao/arquivos/').then((aT) => {
			if (aT.success) {
				this.props.history.push('sucesso');
			}
		});

		return true;
	}

	lattes = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				lattes: files[0] ? files[0] : undefined
			});
		}
	}

	rg = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				rg: files[0] ? files[0] : undefined
			});
		}
	}

	cpf = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				cpf: files[0] ? files[0] : undefined
			});
		}
	}

	elector = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				elector: files[0]
			});
		}
	}

	residenceFile = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				residence: files[0] ? files[0] : undefined
			});
		}
	}

	graduationFront = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				graduationFront: files[0] ? files[0] : undefined
			});
		}
	}

	graduationVerse = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				graduationVerse: files[0] ? files[0] : undefined
			});
		}
	}

	posgraduationFront = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				posgraduationFront: files[0] ? files[0] : undefined
			});
		}
	}

	posgraduationVerse = (e: React.ChangeEvent<HTMLInputElement>) => {
		let files = e.currentTarget.files;
		if (files) {
			this.setState({
				posgraduationVerse: files[0] ? files[0] : undefined
			});
		}
	}

	addFile = () => {
		this.setState((prevState) => ({
			otherFiles: [...prevState.otherFiles, {file: undefined}]
		}));
	}

	removeFile = () => {
		this.setState((prevState) => ({
			otherFiles: prevState.otherFiles.slice(0, -1)
		}));
	}

	otherFilesOnChange = (files: FileList | null, pos: number) => {
		let file = files && files[0] ? files[0] : undefined;
		this.setState((prevState) => ({
			otherFiles: [
				...prevState.otherFiles.slice(0, pos),
				{file: file},
				...prevState.otherFiles.slice(pos + 1)
			]
		}));
	}

	otherFilesName = (pos: number) => {
		let file = this.state.otherFiles[pos].file;
		return file ? file.name : 'Não selecionado';
	}

	isGraduationBothFilesInOne = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.checked;
		this.setState({
			isGraduationBothFilesInOne: value
		});
	}

	isPosgraduationBothFilesInOne = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.checked;
		this.setState({
			isPosgraduationBothFilesInOne: value
		});
	}

	hasPosgraduation = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.checked;
		this.setState({
			hasPosgraduation: !value
		});
	}

	render() {
		return (
			<StyledDiv className="content container inscricao-graduacao-academico">
				<Logo/>
				<div className="container-fluid text-center mb-4">
					<h2>Envio de arquivos</h2>
					<h5>Os arquivos devem enviados apenas em formato PDF</h5>
				</div>
				<ConsoleError text={this.state.errorMsg}/>
				<form action="javascript:void(0)" onSubmit={this.onSubmit}>
					<div className="form-group form-control">
						<label>Curriculum Lattes</label>
						<div className="custom-file mb-1">
							<input type="file" className="custom-file-input" id="lattes" onChange={this.lattes} required={true}/>
							<label className="custom-file-label" htmlFor="lattes">{this.state.lattes ? this.state.lattes.name : 'Não selecionado'}</label>
						</div>
					</div>

					<div className="form-group form-control">
						<label>RG</label>
						<div className="custom-file mb-1">
							<input type="file" className="custom-file-input" id="rg" onChange={this.rg} required={true}/>
							<label className="custom-file-label" htmlFor="rg">{this.state.rg ? this.state.rg.name : 'Não selecionado'}</label>
						</div>
					</div>

					<div className="form-group form-control">
						<label>CPF</label>
						<div className="custom-file mb-1">
							<input type="file" className="custom-file-input" id="cpf" onChange={this.cpf} required={true}/>
							<label className="custom-file-label" htmlFor="cpf">{this.state.cpf ? this.state.cpf.name : 'Não selecionado'}</label>
						</div>
					</div>
					
					<div className="form-group form-control">
						<label>Título de Eleitor</label>
						<div className="custom-file mb-1">
							<input type="file" className="custom-file-input" id="elector" onChange={this.elector} required={true}/>
							<label className="custom-file-label" htmlFor="cpf">{this.state.elector ? this.state.elector.name : 'Não selecionado'}</label>
						</div>
					</div>

					<div className="form-group form-control">
						<label>Comprovante de residência</label>
						<div className="custom-file mb-1">
							<input type="file" className="custom-file-input" id="residenceFile" onChange={this.residenceFile} required={true}/>
							<label className="custom-file-label" htmlFor="residenceFile">{this.state.residence ? this.state.residence.name : 'Não selecionado'}</label>
						</div>
					</div>

					<div className="form-group form-control">
						<label>Diploma de Curso de Graduação{!this.state.isGraduationBothFilesInOne && ' (frente)'}</label>
						<div className="custom-file mb-2">
							<input type="file" className="custom-file-input" id="graduationFront" onChange={this.graduationFront} required={true}/>
							<label className="custom-file-label" htmlFor="graduationFront">{this.state.graduationFront ? this.state.graduationFront.name : 'Não selecionado'}</label>
						</div>
						<label hidden={this.state.isGraduationBothFilesInOne}>Diploma de Curso de Graduação (verso)</label>
						<div className="custom-file mb-2" hidden={this.state.isGraduationBothFilesInOne}>
							<input type="file" className="custom-file-input" id="graduationVerse" onChange={this.graduationVerse} required={!this.state.isGraduationBothFilesInOne}/>
							<label className="custom-file-label" htmlFor="graduationVerse">{this.state.graduationVerse ? this.state.graduationVerse.name : 'Não selecionado'}</label>
						</div>
						<div className="form-check">
							<label className="form-check-label">
								<input className="form-check-input" type="checkbox" onChange={this.isGraduationBothFilesInOne} checked={this.state.isGraduationBothFilesInOne} /> 
								Frente e Verso no mesmo arquivo
							</label>
						</div>
					</div>

					<div className="form-group">
						<div className="form-control" hidden={!this.state.hasPosgraduation}>
							<label>Diploma de Curso de Pós-Graduação{!this.state.isPosgraduationBothFilesInOne && ' (frente)'}</label>
							<div className="custom-file mb-2">
								<input type="file" className="custom-file-input" id="posgraduationFront" onChange={this.posgraduationFront} required={this.state.hasPosgraduation}/>
								<label className="custom-file-label" htmlFor="posgraduationFront">{this.state.posgraduationFront ? this.state.posgraduationFront.name : 'Não selecionado'}</label>
							</div>
							<label hidden={this.state.isPosgraduationBothFilesInOne}>Diploma de Curso de Pós-Graduação (verso)</label>
							<div className="custom-file mb-2" hidden={this.state.isPosgraduationBothFilesInOne}>
								<input type="file" className="custom-file-input" id="posgraduationVerse" onChange={this.posgraduationVerse} required={!this.state.isPosgraduationBothFilesInOne && this.state.hasPosgraduation}/>
								<label className="custom-file-label" htmlFor="posgraduationVerse">{this.state.posgraduationVerse ? this.state.posgraduationVerse.name : 'Não selecionado'}</label>
							</div>
							<div className="form-check">
								<label className="form-check-label">
									<input className="form-check-input" type="checkbox" onChange={this.isPosgraduationBothFilesInOne} checked={this.state.isPosgraduationBothFilesInOne} /> 
									Frente e Verso no mesmo arquivo
								</label>
							</div>
						</div>
						<div className="form-check">
							<label className="form-check-label">
								<input className="form-check-input" type="checkbox" onChange={this.hasPosgraduation} checked={!this.state.hasPosgraduation} /> 
								Não concluí a Pós-Graduação
							</label>
						</div>
					</div>

					{this.state.otherFiles && this.state.otherFiles.map((i, indexI) => {
						return (
							// <div key={indexI}> {indexI}/{i.file ? i.file.name : undefined} </div>
							<div key={indexI} className="form-group">
								<label>Outros arquivos {indexI + 1} </label>
								<div className="custom-file">
									<input type="file" className="custom-file-input" id={'other-files-' + indexI} onChange={(e) => this.otherFilesOnChange(e.currentTarget.files, indexI)} required={true}/>
									<label className="custom-file-label" htmlFor={'other-files-' + indexI}>{this.otherFilesName(indexI)}</label>
								</div>
							</div>
						);
					})}
					<div className="form-group">
						<button type="button" className="btn" onClick={this.addFile} hidden={this.state.otherFiles.length === 3}>Adicionar arquivo extra</button>
						<button type="button" className="btn" onClick={this.removeFile} hidden={this.state.otherFiles.length === 0}>Remover arquivo extra</button>
						<br/><small className="text-muted">Só é possível enviar até 3 arquivos extras.</small>
					</div>
					<br/>
					<LoadingButton 
						isLoading={this.state.isRequesting}
						text="Próximo"
					/>
				</form>
			</StyledDiv>
		);
	}
}