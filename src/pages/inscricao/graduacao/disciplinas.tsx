import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import LoadingButton from '../../../modules/loading-button';
import { RouteComponentProps } from 'react-router-dom';
import { AJAX } from '../../../client/Core';
import ErrorConsole from '../../../modules/error-console';

interface State {
	isRequesting: boolean;
	areas: Option[];
	selects: {
		areaid: string;
		courseindex: number;
		courses: {
			id: string;
			name: string;
			active: '0' | '1';
		}[];
	}[];
	errorMsg: string;
}

type Option = {
	id: string;
	name: string;
};

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			isRequesting: false,
			selects: [{
				areaid: 'default',
				courseindex: 0,
				courses: [this.courseInitialState()]
			}],
			areas: [{
				id: 'default',
				name: 'Selecionar...'
			}],
			errorMsg: ''
		};
	}

	areaInitialState(): State['selects'] {
		return [{
			areaid: 'default',
			courseindex: 0,
			courses: [this.courseInitialState()]
		}];
	}

	courseInitialState(): State['selects'][0]['courses'][0] {
		return {
			id: 'default',
			name: 'Selecionar...',
			active: '1'
		};
	}

	componentDidMount() {
		interface Request {
			success: boolean;
			response: {
				areas: Option[];
			};
		}
		AJAX.request<Request>({
			command: 'get-areas'
		}, '/inscricao/graduacao/disciplinas/').then((aT) => {
			if (aT.success === true) {
				this.setState((prevState) => ({
					areas: [...prevState.areas, ...aT.response.areas]
				}));
			}
		});
	}

	onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		interface Request {
			success?: boolean;
		}

		// Check if subjects repeats:
		for (let i = 0; i < this.state.selects.length; ++i) {
			for (let j = i + 1; j < this.state.selects.length; ++j) {
				if (
					this.state.selects[i].areaid === this.state.selects[j].areaid &&
					this.state.selects[i].courses[this.state.selects[i].courseindex].id === this.state.selects[j].courses[this.state.selects[j].courseindex].id
				) {
					this.setState({
						errorMsg: 'Não é permitido escolher disciplinas iguais'
					});

					return false;
				}
			}
		}

		// Check if subject is default:
		for (let i = 0; i < this.state.selects.length; ++i) {
			if (this.state.selects[i].areaid === 'default' || this.state.selects[i].courses[this.state.selects[i].courseindex].id === 'default') {
				this.setState({
					errorMsg: 'Favor, preencha todos os campos pedidos'
				});
				return false;
			}

			if (this.state.selects[i].courses[this.state.selects[i].courseindex].active === '0') {
				this.setState({
					errorMsg: 'Você não pode se inscrever em cursos que já foram encerrados.'
				});
				return false;
			}
		}

		AJAX.request<Request>({
			command: 'submit-subjects',
			subjects: this.state.selects.map((i, indexI) => {
				return {areaid: i.areaid, courseid: i.courses[i.courseindex].id};
			})
		}, '/inscricao/graduacao/disciplinas/').then((aT) => {
			if (aT.success === true) {
				this.props.history.push('academico');
			}
		});

		return true;
	}

	areaOnChange = (e: React.ChangeEvent<HTMLSelectElement>, pos: number) => {
		let id = e.currentTarget.value;
		this.setState((prevState) => ({
			selects: [
				...prevState.selects.slice(0, pos), 
				{areaid: id, courses: [{id: 'default', name: 'Carregando...', active: '1'}], courseindex: 0}, 
				...prevState.selects.slice(pos + 1)
			]
		}));

		type R = AJAX.Response<{
			courses: State['selects'][0]['courses'];
		}>;

		AJAX.request<R>({
			command: 'get-courses',
			areaid: id
		}, '/inscricao/graduacao/disciplinas/').then((aT) => {
			if (aT.success === true) {
				this.setState((prevState) => ({
					selects: [
						...prevState.selects.slice(0, pos),
						{...prevState.selects[pos], courses: [this.courseInitialState(), ...aT.response.courses]},
						...prevState.selects.slice(pos + 1)
					]
				}));
			}
		});
	}

	courseOnChange = (e: React.ChangeEvent<HTMLSelectElement>, pos: number) => {
		let index = e.currentTarget.selectedIndex;
		this.setState((prevState) => ({
			selects: [
				...prevState.selects.slice(0, pos),
				{...prevState.selects[pos], courses: this.state.selects[pos].courses, courseindex: index},
				...prevState.selects.slice(pos + 1)
			]
		}));
	}

	addSubject = () => {
		if (this.state.selects.length === 3) {
			return;
		}

		this.setState((prevState) => ({
			selects: [...prevState.selects, ...this.areaInitialState()]
		}));
	}

	removeSubject = () => {
		if (this.state.selects.length === 1) {
			return;
		}
		this.setState((prevState) => ({
			selects: prevState.selects.slice(0, -1)
		}));
	}

	render() {
		return (
			<div className="content container">
				<Logo/>
				<div className="container-fluid text-center">
					<h2>Disciplinas</h2>
					<h5>Selecione as disciplinas para as quais gostaria de se inscrever</h5>
					<small>No mínimo 1, no máximo 3</small>
				</div>
				<br/>
				<ErrorConsole text={this.state.errorMsg}/>
				<form action="javascript:void(0)" onSubmit={this.onSubmit}>
				<big className="text-block mb-2" style={{display: 'block'}}>Gostaria de se candidatar para qual vaga?</big>
					{this.state.selects.map((i, indexI) => {
						return (
							<div key={indexI} className="form-group form-control">
								<h4>Disciplina {indexI + 1}</h4>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<label htmlFor={'area-' + indexI} className="input-group-text">Selecionar Curso</label>
									</div>
									<select 
										id={'area-' + indexI} 
										className="custom-select"
										onChange={e => this.areaOnChange(e, indexI)}
										value={this.state.selects[indexI].areaid}
									>
										{this.state.areas.map((j, indexJ) => {
											return <option key={indexJ} value={j.id}>{j.name}</option>;
										})}
									</select>
								</div>
								<div className="input-group">
									<div className="input-group-prepend">
										<label htmlFor={'course-' + indexI} className="input-group-text">Selecionar Disciplina</label>
									</div>
									<select 
										id={'course-' + indexI} 
										className="custom-select"
										value={this.state.selects[indexI].courses[this.state.selects[indexI].courseindex].id}
										onChange={e => this.courseOnChange(e, indexI)}
									>
										{this.state.selects[indexI].courses.map((j, indexJ) => {
											return <option key={indexJ} value={j.id}>{j.name}{j.active === '0' && ' - Inscrições encerradas'}</option>;
										})}
									</select>
								</div>
							</div>
						);
					})}
					<div className="form-group">
						<button type="button" onClick={this.addSubject} className="btn" hidden={this.state.selects.length === 3}>Adicionar Disciplina</button>
						<button type="button" onClick={this.removeSubject} className="btn" hidden={this.state.selects.length === 1}>Remover Disciplina</button>
					</div>
					<LoadingButton 
						isLoading={this.state.isRequesting}
						text="Próximo"
					/>
				</form>
			</div>
		);
	}
}