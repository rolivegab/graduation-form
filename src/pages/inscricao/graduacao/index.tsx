import * as React from 'react';
import { Switch, Route, RouteComponentProps, Redirect } from 'react-router-dom';
import Inicio from './inicio';
import Disciplinas from './disciplinas';
import Vaga from './vaga';
import Academico from './academico';
import Profissional from './profissional';
import Arquivos from './arquivos';
import Sucesso from './sucesso';

export default class extends React.Component<RouteComponentProps<{}>> {
	render() {
		return (
			<Switch>
				<Route path={this.props.match.url + '/inicio'} component={Inicio}/>
				<Route path={this.props.match.url + '/vaga'} component={Vaga}/>
				<Route path={this.props.match.url + '/disciplinas'} component={Disciplinas}/>
				<Route path={this.props.match.url + '/academico'} component={Academico}/>
				<Route path={this.props.match.url + '/profissional'} component={Profissional}/>
				<Route path={this.props.match.url + '/sucesso'} component={Sucesso}/>
				<Route path={this.props.match.url + '/arquivos'} component={Arquivos}/>
				<Redirect from={this.props.match.url} to={this.props.match.url + '/inicio'} exact={true}/>
				<Redirect to={'/not-found'}/>
			</Switch>
		);
	}
}