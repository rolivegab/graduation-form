import * as React from 'react';
import Page from '../../client/Page';
import Logo from '../../modules/unifacexImg';
import './home.css';
import { Link } from 'react-router-dom';

export default class extends Page<{}, {}> {

	render() {
		return (
			<div className="container content home">
				<Logo/>
				<div className="row">
					<div className="col">
						<h2 className="text-center">Bem-vindo!</h2>
						<h5 className="text-center">Este é o sistema de gerenciamento de conteúdo da UNIFACEX!</h5>
					</div>
				</div>
				<br/>
				<div className="row">
					<div className="col-md-6">
						<Link className="clear" to="/cadastro/inicio">
							<div className="inscricao p-4 mb-4">
								<h4 className="text-center">Inscrição</h4>
								<p className="text-justify">Caso queira realizar a sua inscrição no sistema, clique aqui.</p>
							</div>
						</Link>
					</div>
					<div className="col-md-6">
						<Link className="clear" to="/acesso">
							<div className="submissao p-4 mb-4">
								<h4 className="text-center">Acesso</h4>
								<p className="text-justify">Caso já possua cadastro e deseja acessar o painel do sistema, clique aqui.</p>
							</div>
						</Link>
					</div>
				</div>
					<div className="row">
						<div className="col">
							<div className="text-center">Caso tenha alguma dúvida, entrar em contato via e-mail: <b>ead@unifacex.com.br</b></div>
						</div>
					</div>
				</div>
		);
	}
}