import * as React from 'react';
import Logo from '../../modules/unifacexImg';

export default class extends React.Component {
	render() {
		return (
			<div className="container content not-found">
				<Logo/>
				<h2>Arquivo enviado com sucesso!</h2>
			</div>
		);
	}
}