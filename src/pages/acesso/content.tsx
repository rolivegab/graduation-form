import * as React from 'react';
import Logo from '../../modules/unifacexImg';
import { AJAX } from '../../client/Core';
import LoadingButton from '../../modules/loading-button';
import 'react-router';	
import './login.css';
import TextMask from 'react-text-mask';
import { RouteComponentProps } from 'react-router-dom';
import ErrorConsole from '../../modules/error-console';

interface State {
	cpf: string;
	email: string;
	errorMsg: string;
	isLoading: boolean;
}

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			cpf: '',
			email: '',
			errorMsg: '',
			isLoading: false
		};
	}

	onSubmit = (f: React.FormEvent<HTMLFormElement>) => {
		this.setState({
			isLoading: true
		});
		interface Request {
			success?: boolean;
			errorMsg: string;
		}
		AJAX.request<Request>({
			cpf: this.state.cpf.replace(/[\.\-]/g, ''),
			email: this.state.email
		}, '/acesso/').then((aT) => {
			if (aT.success === true) {
				this.setState({
					isLoading: false
				});
				this.props.history.push('/user/panel');
			} else if (aT.success === false) {
				this.setState({
					isLoading: false,
					errorMsg: aT.errorMsg
				});
			}
		});
	}

	onCPFChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			cpf: e.currentTarget.value
		});
	}

	onEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			email: e.currentTarget.value
		});
	}

	render() {
		return (
			<div className="login content container text-center">
				<Logo/>
				<h3>Acesso a sistema</h3>
				<br/>
				<ErrorConsole text={this.state.errorMsg}/>
				<form action="javascript:void(0)" method="POST" onSubmit={this.onSubmit}>
						<label>
							<TextMask
								className="form-control form-control-lg form-control-danger" 
								type="text" 
								name="cpf"
								placeholder="CPF" 
								required={true}
								aria-describedby="cpfError"
								onChange={this.onCPFChange}
								value={this.state.cpf}
								mask={[/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/]}
							/>
						</label><br/>
						<label>
							<input
								className="form-control form-control-lg"
								type="email"
								name="email"
								placeholder="Email"
								required={true}
								aria-describedby="emailError"
								onChange={this.onEmailChange}
								value={this.state.email}
							/>
						</label><br/>
					<br/>
					<br/>
					<LoadingButton
						text={'Próximo'}
						isLoading={this.state.isLoading}
					/>
				</form>
			</div>
		);
	}
}