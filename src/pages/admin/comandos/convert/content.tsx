import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { AJAX } from '../../../../client/Core';
import Loading from '../../../../modules/loading';
import LoadingButton from '../../../../modules/loading-button';

interface State {
	isLoading: boolean;
	isRequesting: boolean;
	step: 1 | 2;
	l_subs: {
		address: string;
		birthday: string;
		cep: string;
		city: string;
		contentmanagerexperience: string;
		cpf: string;
		cpffile: string;
		eadexperience: string;
		electorfile: string;
		email: string;
		employedbefore: string;
		graduationfor: string;
		graduationfilefront: string;
		graduationfileverse: string;
		lattefile: string;
		name: string;
		number: string;
		otherexperiences: string;
		otherfiles1: string;
		otherfiles2: string;
		otherfiles3: string;
		pos_graduation_for: string;
		posgraduationfilefront: string;
		posgraduationfileverse: string;
		residencefile: string;
		rg: string;
		rgfilefront: string;
		rgfileverse: string;
		role: string;
		school_level: string;
		teacherexperience: string;
		tel: string;
	}[];
	m_users: any[];
	actualIndex: number;
}

export default class extends React.Component<RouteComponentProps<{}>, State> {
	paramsUser = ['cpf', 'email'];

	constructor(props: RouteComponentProps<{}>) {
		super(props);
		this.state = {
			isLoading: false,
			isRequesting: false,
			l_subs: [],
			m_users: [],
			actualIndex: 0,
			step: 1
		};
	}

	componentDidMount() {
		this.setState({
			isLoading: true
		});

		AJAX.request<any>({
			command: 'get-l-submissions'
		}, '/admin/comandos/convert/').then((aT) => {
			if (aT.success === true) {
				this.setState({
					l_subs: aT.response.l_subs,
				});
				this.actualizeUser();
			}
		});
	}

	actualizeUser = () => {
		AJAX.request<any>({
			command: 'get-m-users'
		}, '/admin/comandos/convert/').then((aT) => {
			if (aT.success === true) {
				this.setState({
					m_users: aT.response.m_users,
					isLoading: false
				});
			}
		});
	}

	getDiffUser(userid: number) {
		let diff: State['l_subs'] = [];
		let sqlitesubs = this.state.l_subs;
		Object.keys(sqlitesubs).forEach((i, indexI) => {
			this.state.m_users.forEach((j, indexJ) => {
				let exists = false;
				if (sqlitesubs[i].cpf === j.cpf) {
					exists = true;
				}
				if (exists === false) {
					diff.push(sqlitesubs[i]);
				}
			});
		});

		return diff[userid] ? [diff[userid]] : [];
	}

	createUser(cpf: string, email: string) {
		AJAX.request<any>({
			command: 'create-user',
			cpf: cpf,
			email: email
		}, '/cadastro/conta/').then((aT) => {
			if (aT.success === true) {
				this.setState({
					step: 2
				});
			}
		});
	}

	render() {
		return (
			<div>
				{this.state.isLoading ? (
					<div className="container-fluid text-center"><Loading /></div>
				) : (
					this.state.step === 1 && (
						<div className="container-fluid">
							{this.getDiffUser(0).map((i, indexI) => {
								return (
									<p className="small mb-5" key={indexI}>
										{this.paramsUser.map((j, indexJ) => {
											return (
												<div key={indexJ}>
													<label htmlFor="">
														{j}
														<input 
															type="text" 
															className="ml-4" 
															value={i[j]}
														/>
													</label>
												</div>
											);
										})}
									</p>
								);
							})}
							<LoadingButton isLoading={this.state.isRequesting} text={'Criar usuário'} onClick={e => this.createUser('a', 'b')}/>
							{/* <LoadingButton isLoading={this.state.isRequesting} text={'Criar usuário'} onClick={}/> */}
						</div>
					) ||
					this.state.step === 2 && (
						<div> step2 </div>
					)
				)}
			</div>
		);
	}
}