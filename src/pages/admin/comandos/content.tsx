import * as React from 'react';
import styled from 'styled-components';
import Convert from './convert/content';
import { Redirect, Route, Switch, RouteComponentProps } from 'react-router';

const StyledDiv = styled.div`

`;

interface State {
	isLoading: boolean;
}

export default class extends React.Component<RouteComponentProps<{}>, State> {

	actualPage = () => {
		return (
			<div> oi </div>
		);
	}

	render() {
		return (
			<StyledDiv className="container content">
				<Switch>
					<Route path={this.props.match.url} render={this.actualPage} exact={true}/>
					<Route path={this.props.match.url + '/convert'} component={Convert}/>
					<Route path={this.props.match.url + '/teste'} render={() => (<div> oi </div>)}/>
					<Redirect to="/not-found"/>
				</Switch>
			</StyledDiv>
		);
	}
}