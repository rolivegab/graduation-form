import * as React from 'react';
import AJAX from '../../../client/Core';
import './submissao.css';

interface State {
	contents: {
		cpf: string; 
		name: string;
		files: {
			id: string;
			filebasename: string;
			fileextension: string;
			filekey: string;
		}[]
	}[];
}

export default class extends React.Component<{}, State> {
	constructor(props: {}) {
		super(props);
		this.state = {
			contents: []
		};
	}

	componentWillMount() {
		AJAX.request<{
			contents: any[]}>({
				command: 'content'
			},
			'/admin/submissao/'
		).then((aT) => {
			if (aT.contents) {
				let contents = aT.contents;
				contents.forEach((i, indexI) => {
					AJAX.request(
						{
							command: 'files',
							cpf: i.cpf
						},
						'/admin/submissao/'
					).then((aT2: any) => {
						if (aT2.files) {
							let files = aT2.files as any[];
							this.setState(prevState => ({
								contents: [...prevState.contents, {cpf: i.cpf, name: i.name, files: files}]
							}));
						}
					});
				});
			}
		});
	}

	render() {
		return (
			<div className="container content submissao">
				<table>
					<thead>
						<tr>
							<th>CPF</th>
							<th>Nome</th>
							<th>Files</th>
						</tr>
					</thead>
					<tbody>
						{this.state.contents.map((i, indexI) => {
							return (
								<tr key={indexI}>
									<td>{i.cpf}</td>
									<td>{i.name}</td>
									<td>
										{i.files.map((j, indexJ) => {
											return (
												<small key={indexJ}>
													{indexJ + 1}- <a href={AJAX.serverURI + '/admin/submissao/baixarArquivo/?arquivo=' + j.id} target="_blank">{j.filebasename}</a><br/>
												</small>
											);
										})}
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		);
	}
}