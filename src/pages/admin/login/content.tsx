import * as React from 'react';
import Logo from '../../../modules/unifacexImg';
import { AJAX } from '../../../client/Core';
import './login.css';
import * as queryString from 'query-string';
import { RouteComponentProps } from 'react-router-dom';
import ErrorConsole from '../../../modules/error-console';

interface State {
	username: string;
	password: string;
	errorMsg: string;
}

export default class extends React.Component<RouteComponentProps<{}>, State> {
	constructor(props: RouteComponentProps<{}>) {
		super(props);

		this.state = {
			username: '',
			password: '',
			errorMsg: ''
		};
	}

	isLoginSuccess() {
		let parsed = queryString.parse(location.search);
		if (parsed.success === 'false') {
			return false;
		} else {
			return true;
		}
	}

	onSubmit = () => {
		AJAX.request<{success: boolean}>(
			{
				command: 'admin-login',
				username: this.state.username,
				password: this.state.password
			}, '/admin/'
		).then((aT) => {
			if (aT.success === true) {
				this.props.history.push('panel');
			} else {
				this.setState({
					errorMsg: 'Usuário ou senha incorretos.'
				});
			}
		});
	}

	username = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			username: value
		});
	}

	password = (e: React.ChangeEvent<HTMLInputElement>) => {
		let value = e.currentTarget.value;
		this.setState({
			password: value
		});
	}

	render() {
		return (
			<div className="container content login">
				<form className="form-group" action="javascript:void(0)" onSubmit={this.onSubmit}>
					<Logo/>
					<div className="container-fluid text-center"><h2>Login</h2></div><br/>
					<br/>
					<ErrorConsole text={this.state.errorMsg}/>
					<small className="form-text incorrect" id="cpfError" hidden={this.isLoginSuccess()}>
						CPF incorreto ou não-existente.
					</small>
					<label className="required-right">
						<input 
							className="form-control form-control-lg"
							type="text"
							name="username" 
							placeholder="Usuário"
							value={this.state.username}
							onChange={this.username}
							required={true}
						/>
					</label><br/>
					<label className="required-right">
						<input 
							className="form-control form-control-lg"
							type="password" 
							name="password" 
							placeholder="Senha" 
							value={this.state.password}
							onChange={this.password}
							required={true}
						/>
					</label><br/>
					<br/><br/>
					<button className="btn btn-block btn-lg" type="submit">Autenticar</button>
				</form>
			</div>
		);
	}
}