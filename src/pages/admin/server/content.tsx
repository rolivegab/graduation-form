import * as React from 'react';
import './server.css';
import Loader from '../../../modules/loading';
import * as ToggleSwitch from '../../../modules/switch';
import MiniLoader from '../../../modules/mini-loading';
import AJAX from '../../../client/Core';

interface Configuration {
	isLoading: boolean;
	value?: boolean;
}

interface State {
	loading: boolean;
	maintenance: Configuration;
	inscription: Configuration;
}

export default class extends React.Component<{}, State> {

	constructor(props: {}) {
		super(props);
		this.state = {
			loading: false,
			maintenance: {
				isLoading: false
			},
			inscription: {
				isLoading: false
			}
		};

		this.changeMaintenance = this.changeMaintenance.bind(this);
		this.changeInscription = this.changeInscription.bind(this);
	}

	// Carrega o valor de cada configuração inicial, verifica se o sistema está em manutenção
	componentWillMount() {
		this.setState({
			loading: true
		});
		AJAX.request(
			{
				command: 'configurations'
			}, 
			'/admin/server/'
		).then((aT: any) => {
			if (aT.configurations) {
				let c = aT.configurations;
				this.setState({
					maintenance: {
						isLoading: false,
						value: c.maintenance	
					},
					inscription: {
						isLoading: false,
						value: c.inscription
					},
					loading: false
				});
			}
		});
	}

	changeMaintenance(checked: boolean) {
		this.setState(prevState => ({
			maintenance: {
				...prevState.maintenance,
				isLoading: true
			}
		}));
		AJAX.request(
			{
				command: 'changeConfiguration',
				name: 'maintenance',
				value: checked
			},
			'/admin/server/'
		).then((aT: any) => {
			if (aT.success === true) {
				this.setState({
					maintenance: {
						isLoading: false,
						value: checked
					}
				});
			}
		});
	}

	changeInscription(checked: boolean) {
		this.setState(prevState => ({
			inscription: {
				...prevState.inscription,
				isLoading: true
			}
		}));
		AJAX.request<{
			success: boolean
		}>({
			command: 'changeConfiguration',
			name: 'inscription',
			value: checked
		},
		'/admin/server/').then((aT) => {
			if (aT.success === true) {
				this.setState({
					inscription: {
						isLoading: false,
						value: checked
					}
				});
			}
		});
	}

	renderConfiguration(props: ToggleSwitch.Props, value?: boolean) {
		if (value === undefined) {
			return null;
		} else {
			return (
				<ToggleSwitch.default 
					{...props}
					value={value}
				/>
			);
		}
	}

	renderContent() {
		if (this.state.loading) {
			return <div className="container-fluid text-center"><Loader/></div>;
		} else {
			return (
				<div>
					<label>
						Modo de manutenção: <br/>
						{this.renderConfiguration(
							{
								onClick: this.changeMaintenance
							},
							this.state.maintenance.value
						)}
						<div 
							className="mini-loader"
							hidden={!this.state.maintenance.isLoading}
						>
							<MiniLoader width={30} height={30}/>
						</div>
					</label><br/>

					<label>
						Inscrição de novos usuários: <br/>
						{this.renderConfiguration(
							{
								onClick: this.changeInscription
							},
							this.state.inscription.value
						)}
						<div 
							className="mini-loader"
							hidden={!this.state.inscription.isLoading}
						>
							<MiniLoader width={30} height={30}/>
						</div>
					</label>
				</div>
			);
		}
	}

	render() {
		return (
			<div className="container content server">
				<h2 className="container-fluid text-center">Configurações do SERVIDOR</h2>
				<br/>
				{this.renderContent()}
			</div>
		);
	}
}