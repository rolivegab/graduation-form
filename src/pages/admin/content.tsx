import { Switch, Route, Redirect } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';
import * as React from 'react';
import Login from './login/content';
import Panel from './panel/content';
import Server from './server/content';
import Commands from './comandos/content';

export default class extends React.Component<RouteComponentProps<{}>> {
	render() {
		return (
			<Switch>
				<Route path={this.props.match.url + '/login'} component={Login}/>
				<Route path={this.props.match.url + '/panel'} component={Panel}/>
				<Route path={this.props.match.url + '/server'} component={Server} />
				<Route path={this.props.match.url + '/comandos'} component={Commands} />
				<Redirect from={this.props.match.url} to={this.props.match.url + '/login'} exact={true}/>
				<Redirect to={'/not-found'} exact={true}/>
			</Switch>
		);
	}
}