import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const StyledDiv = styled.div`
	@media (min-width: 992px) {
		&.container{
			max-width: 960px;
		}	
	}

	.usuarios {
		color: white;
		background-color: #133b6c;
	}
	
	.submissao {
		color: white;
		background-color: #d0332d;
	}

	.servidor {
		color: white;
		background-color: #3e3e3e;
	}

	.icon {
		font-size: 35px;
	}
`;

export default class extends React.Component {
	render() {
		return (
			<StyledDiv className="container content options">
				<h2 className="container-fluid text-center">Bem-vindo, ADMINISTRADOR!</h2>
				<h5 className="container-fluid text-center">Selecione a opção desejada</h5>
				<br/>
				<div className="row justify-content-around">
					<div className="col-md-6">
						<Link to="server" className="clear">
							<div className="servidor p-4 mb-4">
								<p className="icon font-awesome text-center">&#xf233;</p>
								<p className="text-center">Gerenciar configurações do servidor</p>
							</div>
						</Link>
					</div>
					<div className="col-md-6">
						<Link to="submissao" className="clear">
							<div className="usuarios p-4 mb-4">
								<p className="icon font-awesome text-center">&#xf0c0;</p>
								<p className="text-center">Ver e editar usuários do sistema</p>
							</div>
						</Link>
					</div>
				</div>
				<div className="row justify-content-around">
					<div className="col-md-6">
						<Link to="comandos" className="clear">
							<div className="inscricao p-4 mb-4">
								<h4 className="text-center">Comandos</h4>
								<p className="text-justify">Comandos específicos de coneversão de banco de dados e integração de sistemas.</p>
							</div>
						</Link>
					</div>
					<div className="col-md-6">
						<Link to="tanto-faz" className="clear">
							<div className="submissao p-4 mb-4">
								<h4 className="text-center">Submissões</h4>
								<p className="text-justify">Caso queira ver dados de submissões enviadas, clique aqui.</p>
							</div>
						</Link>
					</div>
				</div>
			</StyledDiv>
		);
	}
}